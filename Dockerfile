FROM python:3.9-slim-bullseye

COPY Pipfile* ./

RUN pip install pipenv \
 && pipenv install --deploy --system \
 && pip cache purge \
 && rm Pipfile*

COPY src .

CMD python app.py