from pytest import CaptureFixture
from app import fizz_buzz
import pathlib

def test_stdout_redirect(capsys: CaptureFixture):
    print('pippo')

    captured = capsys.readouterr()
    assert captured.out == 'pippo\n'

def test_golden_master(capsys: CaptureFixture):
    fizz_buzz()

    captured = capsys.readouterr()

    with open(pathlib.Path(__file__).parent.resolve().joinpath('golden.txt')) as f:
        expected = f.read()
        assert captured.out == expected