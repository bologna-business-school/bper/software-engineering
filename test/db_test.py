from connection import engine
from os import environ
import pytest
import pandas as pd
from sqlalchemy.engine import Engine

@pytest.fixture
def conn() -> Engine:
    return engine(environ.get("DB_CONN", "db_user:db_pass@db/db_name"))

@pytest.mark.db
def test_db(conn: Engine):
    df = pd.read_sql('select 1, now()', con=conn)
    assert len(df.columns) == 2
