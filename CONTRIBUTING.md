# Sample App

## Dipendenze

E' necessario python 3.x

## Esecuzione

Per eseguire l'applicazione digitare:

``` bash
$ cd src
$ python app.py
```

## Formatting

Per formattare un file da linea di comando:

``` bash
$ pipenv run format app/app.py
```

# Dockerizzazione

L'applicazione prevede di poter essere dockerizzata.

Per buildare l'immagine eseguire:

``` bash
$ docker build -t sample .
```

Per eseguire l'immagine:

``` bash
$ docker run sample
```