import sqlalchemy as sa
from sqlalchemy.engine import Engine

def engine(conn_string: str) -> Engine:
    return sa.create_engine(f"postgresql+pg8000://{conn_string}", client_encoding='utf8')